use crate::repo_manifest::{RepoManifest, RepoProject};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use std::path::{Path, PathBuf};
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct GlProject {
    pub fetch_url: String,
    pub path: String,
    pub revision: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct GlProjects {
    pub projects: HashMap<String, GlProject>,
}

pub(crate) struct KeyAndProject(String, GlProject);
/// Returns a KeyAndGlProject
/// Argument tuple:
/// `path_to_`
/// `repo_project`
impl<P: AsRef<Path>> TryFrom<(&P, &RepoProject)> for KeyAndProject {
    type Error = std::io::Error;
    fn try_from(args: (&P, &RepoProject)) -> Result<Self, std::io::Error> {
        let mut project = GlProject::default();
        let name = PathBuf::from(&args.1.name);
        let mut path = PathBuf::from(args.0.as_ref());
        path.push(&name);
        let name = name.file_name().unwrap().to_str().unwrap();
        project.fetch_url = String::from(path.to_str().unwrap());
        project.path = args.1.path.clone();
        project.revision = args.1.revision.clone();
        Ok(KeyAndProject(name.into(), project))
    }
}

/// Returns a GlProjects data structure from a specified repo manifest.
/// # Arguments
///
/// * `manifest_file` full path to repo manifest.
///
impl TryFrom<&Path> for GlProjects {
    type Error = std::io::Error;
    fn try_from(manifest_file: &Path) -> Result<Self, Self::Error> {
        let s = std::fs::read_to_string(manifest_file)?;
        let m = quick_xml::de::from_str::<RepoManifest>(&s).unwrap();
        let mut projects = GlProjects::default();
        for p in &m.projects {
            let kp = KeyAndProject::try_from((&m.remotes[0].fetch, p)).unwrap();
            let (name, project) = (kp.0, kp.1);
            projects.projects.insert(name, project);
        }
        Ok(projects)
    }
}

impl fmt::Display for GlProject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "|Fetch   |{:<70}|", self.fetch_url)?;
        writeln!(f, "|Path    |{:<70}|", self.path)?;
        writeln!(f, "|Revision|{:<70}|", self.revision)
    }
}

impl fmt::Display for GlProjects {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (name, project) in &self.projects {
            writeln!(f, "# {}", name)?;
            writeln!(f, "")?;
            writeln!(f, "{}", project)?;
        }
        write!(f, "")
    }
}
