use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct RepoRemote {
    pub fetch: String,
    pub name: String,
    #[serde(default = "String::default")]
    pub review: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct RepoDefault {
    pub remote: String,
    #[serde(default = "String::default")]
    pub revision: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct RepoProject {
    pub name: String,
    pub path: String,
    #[serde(default = "String::default")]
    pub revision: String,
    #[serde(default = "String::default")]
    pub upstream: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct RepoManifest {
    #[serde(rename = "remote", default)]
    pub remotes: Vec<RepoRemote>,
    pub default: RepoDefault,
    #[serde(rename = "project", default)]
    pub projects: Vec<RepoProject>,
}

#[derive(Serialize, Deserialize, Debug)]
struct RepoRoot {
    manifest: RepoManifest,
}
