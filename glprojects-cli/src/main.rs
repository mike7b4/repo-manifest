use clap::{ArgEnum, Parser};
use repo_manifest::GlProjects;
use std::path::PathBuf;
#[derive(ArgEnum, Clone)]
enum Format {
    Yaml,
    Md,
}

#[derive(Parser)]
#[clap(version)]
struct Args {
    /// Manifest file to parse
    #[clap(short, long)]
    manifest_file: PathBuf,
    /// Optional output file.
    #[clap(short, long)]
    output_file: Option<PathBuf>,
    /// Replace output file if already exists.
    #[clap(short, long)]
    replace: bool,
    /// format
    #[clap(short, long, default_value = "md", arg_enum)]
    format: Format,
}

fn main() {
    let _ = simple_logger::init();
    let mut args = Args::parse();
    args.manifest_file = args.manifest_file.canonicalize().unwrap();
    if let Some(out) = &mut args.output_file {
        if out.as_path().exists() {
            *out = out
                .canonicalize()
                .unwrap_or_else(|e| panic!("Could not expand: '{}' cause: '{}'", out.display(), e));
            if *out == args.manifest_file {
                panic!("Meeh manifest file and output file can not be the same!");
            }
            if !args.replace {
                panic!(
                    "'{}' already exists add --replace if you want to overwrite it.",
                    out.display()
                );
            }
        }
    }

    let glprojects = GlProjects::try_from(args.manifest_file.as_ref())
        .unwrap_or_else(|e| panic!("Read manifest failed cause: '{}'.", e));
    let out = match &args.format {
        Format::Yaml => serde_yaml::to_string(&glprojects).unwrap(),
        Format::Md => format!("{}", &glprojects),
    };
    if let Some(file) = &args.output_file {
        std::fs::write(file, out)
            .unwrap_or_else(|e| panic!("Could not write to: '{}' cause: '{}'", file.display(), e));
    } else {
        println!("{}", out);
    }
}
